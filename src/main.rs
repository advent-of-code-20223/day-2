use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("input.txt").unwrap();
    let reader = BufReader::new(file);
    let win_combinations = vec!["A Y", "B Z", "C X"];
    let draw_combinations = vec!["A X", "B Y", "C Z"];
    let mut sum = 0;
    for line in reader.lines() {
        let l = &(line.unwrap()[..]);
        if win_combinations.contains(&l) {
            sum += 6;
        }
        else if draw_combinations.contains(&l) {
            sum += 3;
        }
        if l.ends_with("X") {
            sum += 1
        }
        else if l.ends_with("Y") {
            sum += 2
        }
        else if l.ends_with("Z") {
            sum += 3
        }
    }
    println!("{}", sum);
}

fn get_score(input: &str) -> u32 {
    
}
